module.exports ={
    iconFont: {
        svgSrc: 'src/assets/iconfont/svgs/*.svg', // path to the svg files to convert
        fontName : 'ROThemeFont', // name of the font family
        scssTemplatePath: 'src/assets/iconfont/templates/_icons.scss', // template of the mixin
        scssFileDestination: '../../scss/_fonts.scss', //relative path to the gulp.dest
        fontDestination: 'src/assets/iconfont/fonts/', // where to dump the font files after converting them from svg
        fontPath: '../iconfont/fonts/' //relative from the css folder that the scss compiles to
    }
}
