var gulp        = require('gulp');
var iconfont    = require('gulp-iconfont');
var iconfontCss = require('gulp-iconfont-css');
var config      = require('../../config').iconFont;

gulp.task('iconfont', function(){
    return gulp.src(config.svgSrc)
        .pipe(iconfontCss({
            fontName: config.fontName,
            path: config.scssTemplatePath,
            targetPath: config.scssFileDestination,
            fontPath: config.fontPath
        }))
        .pipe(iconfont({
            fontName: config.fontName
        }))
        .pipe(gulp.dest(config.fontDestination));
});
