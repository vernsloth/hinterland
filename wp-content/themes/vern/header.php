<!doctype html>
<html class="no-js" <?php language_attributes(); ?> >
	<head>
		<meta charset="<?php bloginfo( 'charset' ); ?>" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<link href="https://fonts.googleapis.com/css?family=Abel" rel="stylesheet">
		<?php wp_head(); ?>
	</head>
	<body <?php body_class(); ?>>
	<?php do_action( 'after_opening_body' ); ?>

	<header class="site-header" role="banner">
		<a href="/" rel="home" class="logo" role="heading" aria-label="logo">
			<img src="https://hinterland.vkelly.com/wp-content/uploads/2018/12/logo-white.png">
		</a>

		<nav class="site-navigation" role="navigation">
			<?php wp_nav_menu(array('menu' => __( 'Right Top Bar', 'vern' ), 'menu_class' => 'toparea',)); ?>
			<?php wp_nav_menu(array('menu' => __( 'Hamburger', 'vern' ), 'menu_class' => 'mobile',)); ?>
			<button aria-label="Main Menu" class="hamburger hamburger--spin-r" type="button" role="button" aria-controls="navigation" rel="home">
			  <span class="hamburger-box">
			    <span class="hamburger-inner"></span>
			  </span>
			</button>
		</nav>
	</header>
