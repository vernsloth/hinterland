<?php
/*
Template Name: Front
*/
get_header(); ?>
<?php do_action( 'foundationpress_before_content' ); ?>
<?php while ( have_posts() ) : the_post();
	$featured_image_L = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'fp-xlarge' )[0];
	$featured_image_M = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'fp-medium' )[0];
	?>
	<div class="hero" data-interchange="[<?php echo $featured_image_M; ?>, small], [<?php echo $featured_image_L; ?>, large]">
		<div class="marketing align-center">
			<div class="anim-hero">
				<h5 class="white">Tailored Skis</h5>
				<span class="wipe"><img src="https://hinterland.vkelly.com/wp-content/uploads/2018/12/text.png"></span>
			</div>
		</div>
	</div>

	<div class="grid-container">
		<h1 class="h1 text-center mrg-btm-60"><span><?php the_field('sub_heading');?></span> <?php the_field('heading');?></h1>
		<div class="grid-x grid-margin-x">
			<div class="cell medium-4 large-offset-2 white">
				<?php the_field('left_paragraph');?>
			</div>
			<div class="cell medium-4 white">
				<?php the_field('right_paragraph');?>
			</div>
		</div>
		<div class="text-center pad40 mrg-btm-40">
			<a href="<?php the_field('button_url');?>" class="btn"><?php the_field('button_text');?></a>
		</div>
	</div>

	<div class="logo-transitions white">
		<div class="grid-container">
			<h3 class="h1 text-center mrg-btm-40"><span><?php the_field('gallery_sub_heading');?></span><?php the_field('gallery_heading');?></h3>
			<div class="grid-x small-up-2 medium-up-4 large-up-6 text-center align-middle">
			<?php
			$images = get_field('client_logos');
			$photocount=0; foreach( $images as $image ):
	            $photocount++;
	            if($photocount <= 6) {  ?>
					<div class="cell">
	                    <img src="<?php echo $image['sizes']['large']; ?>" alt="<?php echo $image['alt']; ?>" title="<?php echo $image['title']; ?>" />
					</div>
	       		<?php }
	        	endforeach;
			    ?>
			</div>
		</div>
	</div>

<?php endwhile; ?>
<?php do_action( 'foundationpress_after_content' ); ?>
<?php get_footer();
