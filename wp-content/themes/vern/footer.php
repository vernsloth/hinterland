
<footer class="footer">
    <div class="footer-container">
        <div class="anime-icon">
          <img src="https://hinterland.vkelly.com/wp-content/uploads/2018/12/logo-white.png">
        </div>
        <div class="grid-x">
           <div class="cell small-12 medium-6">
           		<a href="#">801.000.0000 | Contact Us</a>
           </div>
           <div class="cell small-12 medium-6 text-right">
           		<a href="#" class="sm">123 Street Address for the Shop</a>
           </div>
        </div>
        <div class="grid-x">
        	<div class="cell small-12 text-center mrg-top-60">
            <div class="social-icons">
                <?php if( get_field('facebook', 'option') ): ?><a class="social" href="<?php the_field('facebook', 'option'); ?>" target="_blank"><i class="icon-facebook"></i></a><?php endif; ?>
                <?php if( get_field('twitter', 'option') ): ?><a class="social" href="<?php the_field('twitter', 'option'); ?>" target="_blank"><i class="icon-twitter"></i></a><?php endif; ?>
                <?php if( get_field('linkedin', 'option') ): ?><a class="social" href="<?php the_field('linkedin', 'option'); ?>" target="_blank"><i class="icon-linkedin"></i></a><?php endif; ?>
                <?php if( get_field('youtube', 'option') ): ?><a class="social" href="<?php the_field('youtube', 'option'); ?>" target="_blank"><i class="icon-youtube"></i></a><?php endif; ?>
                <?php if( get_field('google', 'option') ): ?><a class="social" href="<?php the_field('google', 'option'); ?>" target="_blank"><i class="icon-google"></i></a><?php endif; ?>
                <?php if( get_field('pinterest', 'option') ): ?><a class="social" href="<?php the_field('pinterest', 'option'); ?>" target="_blank"><i class="icon-pinterest"></i></a><?php endif; ?>
                <?php if( get_field('vimeo', 'option') ): ?><a class="social" href="<?php the_field('vimeo', 'option'); ?>" target="_blank"><i class="icon-vimeo"></i></a><?php endif; ?>
                <?php if( get_field('instagram', 'option') ): ?><a class="social" href="<?php the_field('instagram', 'option'); ?>" target="_blank"><i class="icon-instagram"></i></a><?php endif; ?>
            </div>
            <div class="copyright">
        		  @<?php echo the_date('Y'); ?> hinterland. All Rights Reserved
            </div>
        	</div>
        </div>
    </div>
</footer>

<?php wp_footer(); ?>
</body>
</html>
