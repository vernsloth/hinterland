
jQuery(document).ready(function($) {

 function updateViewportDimensions() {
        var w=window,d=document,e=d.documentElement,g=d.getElementsByTagName('body')[0],x=w.innerWidth||e.clientWidth||g.clientWidth,y=w.innerHeight||e.clientHeight||g.clientHeight;
        return { width:x,height:y };
    }
    // setting the viewport width
    var viewport = updateViewportDimensions();


	if( viewport.width >= 1200 ) {
		var viewheight = viewport.height;
		$('.hero').height(viewheight); 
	} 
	$('.hamburger').on('click', function(){
        $(this).toggleClass('is-active');
        $('.menu-hamburger-container').toggleClass('js-slideover');
    });	

  $(window).scroll(function(){
	  if ($(window).scrollTop() >= 60) {
	    $('.site-header').addClass('scrolled');
	   }
	   else {
	    $('.site-header').removeClass('scrolled');
	   }
	});

	setTimeout(function(){
	  $('.logo, .anim-hero h5').addClass('js-animate');
	}, 500);

	setTimeout(function(){
	  $('.anim-hero .wipe').addClass('js-animate');
	  $('.play-vid').addClass('js-animate');
	}, 1500);


	$('.fancybox').fancybox({
		padding: 0,
	});

  	if( $('#scene').length ){
		var scene = $('#scene').get(0);
		var parallaxInstance = new Parallax(scene);
	}

	 var options = {
	 	id: 59777392,
        muted: true,
	    background: true,
	    loop: true,
    };

    if( $('.video-play').length ){
		var musicVideoIframe = document.querySelector('#my-video-iframe');
		var musicPlayer = new Vimeo.Player(musicVideoIframe, options);
		musicPlayer.setCurrentTime(0);
	}







	new fullpage('#fullpage', {
		offsetSections: true,
		scrollOverflow: true,
		responsiveSlides: true,
        responsiveWidth: 759,
        normalScrollElementTouchThreshold: 3,
		licenseKey: 'A6E08093-4EA14395-8C64968C-D1280426',
		offsetSectionsKey: 'cmVkb2xpdmUucmVkb2xpdmUuY29fV1V0YjJabWMyVjBVMlZqZEdsdmJuTT1RNU8=',

	
		afterLoad: function(origin, destination, direction) {
			if($('body').hasClass('fp-viewing-0')){
				$('.site-header').removeClass('scrolled');
			}

			if($('.video-play').hasClass('active')){
				 musicPlayer.play();
			}
		},
		onLeave: function(origin, destination, direction) {
			if($('.video-play').hasClass('active')){
				 musicPlayer.pause();
			}
			if($('body').hasClass('fp-viewing-0')){
				$('.site-header').addClass('scrolled');
			}
		}
	});
	
	
});
