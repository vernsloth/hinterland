<?php
/**
 * Add the browser sync snippet
 */

if ( ! function_exists( 'ro_add_browsersync' ) ) :
function ro_add_browsersync() {
	if( ! isset( $_SERVER['HTTP_HOST'] ) ) return '';

	if( ! strstr( $_SERVER['HTTP_HOST'], 'local.' ) && ! strstr( $_SERVER['HTTP_HOST'], '.dev' ) ){
		return '';
	}

	echo '<script id="__bs_script__">//<![CDATA[
		document.write("<script async src=\'http://HOST:3000/browser-sync/browser-sync-client.js?v=2.18.13\'><\/script>".replace("HOST", location.hostname));
		//]]></script>';
}
add_action('foundationpress_before_closing_body', 'ro_add_browsersync' );
endif;