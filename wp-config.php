<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //

/** Set the HTTP host when running things from the CLI **/
if( defined('WP_CLI') && WP_CLI ) $_SERVER['HTTP_HOST'] = 'local.hinterland.com'; //e.g. local.redolive.com

/** Find config file or use default DB config **/
$config_file = dirname( __FILE__ ) . '/_config/' . $_SERVER['HTTP_HOST'] . '.php';
if( file_exists( $config_file ) ) require_once $config_file;

if( ! defined( 'DB_NAME' ) ) 		define('DB_USER', 'hinterland');
if( ! defined( 'DB_USER' ) ) 		define('DB_USER', '');
if( ! defined( 'DB_PASSWORD' ) )	define('DB_PASSWORD', '');
if( ! defined( 'DB_HOST' ) )		define('DB_HOST', 'localhost');
if( ! defined( 'DB_CHARSET' ) )		define('DB_CHARSET', 'utf8');
if( ! defined( 'DB_COLLATE' ) )		define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '>m5#&mu+M+dfo+o2 SYdJ+e33b9.N,F*_d83:f[-Gu8@,C{vOHX/|@hu#R|ONCZ)');
define('SECURE_AUTH_KEY',  ']a%E1cBM+GUn]_][Gjs.3o+Fm3/O)0}AM*ksmZf2+#7i[b&K*ghM[X(%|$RuG7-5');
define('LOGGED_IN_KEY',    'a-;>T4miKH^B9g&BD4TQ.562|h{1ZxdFUm|xMR>@|6:B%~S^P+HuNkz{X/B6Xy g');
define('NONCE_KEY',        'c+c.q]qbe}8.f-Q}+}+=-~mXV;`D4CS0Rd]JF-D0rDJdlpQzD6alk(>Lh`u1?a_N');
define('AUTH_SALT',        '8sTK;4 L-wQ9xD+l|-$Ppte0r%_i=hn8!@`uYE9ch7x,)-e.e-/d@~9yn!C+01{Q');
define('SECURE_AUTH_SALT', 'A-e)Q@@sC?F3@c0:2aDZ3CA #O:ezZ|<xs7B9~W&:^ezhF%KXCSL4N`it7l &?LP');
define('LOGGED_IN_SALT',   '@%F^h{L*_yS&~W8+od{ AV`R VA8-x^uN^IhAS}?] +)w)-j;#8g3I++eJ_-aU_N');
define('NONCE_SALT',       'o1O)i*YN+243V1|tpdU=`*&}yQVwEI;Vun4m@!`&:$D_;.|x.}/sJ_qVT?Yc+p!,');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'hint_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */

if( ! defined( 'WP_DEBUG' ) ) define( 'WP_DEBUG', true );
if( ! defined( 'WP_DEBUG_LOG' ) ) define( 'WP_DEBUG_LOG', true );
if( ! defined( 'WP_DEBUG_DISPLAY' ) ) define( 'WP_DEBUG_DISPLAY', false );

if( ! defined( 'WP_SITEURL' ) ) define( 'WP_SITEURL', 'https://' . $_SERVER['HTTP_HOST'] . '/' );
if( ! defined( 'WP_HOME' ) ) define( 'WP_HOME', 'https://' . $_SERVER['HTTP_HOST'] . '/' );

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) )
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
